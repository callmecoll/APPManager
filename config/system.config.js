var root_path = require('path').resolve(__dirname, '..').replace(/\\/g, "/");

/**
 *  系统配置文件
 */
module.exports = {
	system:{
		version: "2016.1.3",
		/**
		 * 服务名称
		 */
		name:"base",
		/**
		 *  系统后台文件路径
		 */
		src_path: root_path + "/src/",
		/**
		 * 是否启用自定义路由
		 */
		custom_route: false,
		/**
		 * 全局变量名
		 */
		global_variable_name: "$"
	},
	http:{
		/**
		 *  HTTP端口
		 */
		port: 56676,
		/**
		 *  游览器ico路径
		 */
		ico: "",
		/**
		 *  首页
		 */
		init_url: "/index/init",
		/**
		 *  资源正则
		 */
		resources_regular: /.*?(.bmp|.jpg|.png|.gif|.jpeg|.js|.css|.otf|.eot|.svg|.ttf|.woff|.woff2|.json|.swf|.map|.zip|.rar|.7z|.txt)$/,
		/**
		 *  资源默认路径
		 */
		resources_path: root_path,
		/**
		 *  view默认路径
		 */
		view_path: root_path + "/view",
		/**
		 *  默认调用函数（当调用函数为空时，默认调用函数名）
		 */
		default_action:"init",
		/**
		 * 是否开启http服务
		 */
		enable: true
	},
	hp:{
		/**
		 *  使用协议
		 */
		agreement: "ws",
		/**
		 *  端口
		 */
		port: 56670,
		/**
		 *  是否启用推送服务
		 */
		publish: true,
		/**
		 * 是否开启hp服务
		 */
		enable: true
	},
	mongo:{
		address:[
			{
				name:"default",
				address: "192.168.1.188",
				port: 27017,
				cfg: {auto_reconnect: true, poolSize: 10}
			},
			{
				name: "file",
				address:"192.168.1.188",
				port: 27017,
				cfg: {auto_reconnect: true, poolSize: 10}
			}
		]
	},
	vm:{
		/**
		 * 禁止加载的路径
		 */
		ban_load_package:[
			root_path + "/src/",
			root_path + "/base/"
		]
	}
	
};