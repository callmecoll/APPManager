var ch_self = null;
var cnynld_hprose = {
	address:"localhost",
	port: 56670,
	client: null,
	pushs:{},
	init:function () {
		try{
			ch_self = this;
			ch_self.client = hprose.Client.create('ws://' + this.address + ':' + this.port, ['call']);
			ch_self.client.subscribe("push", function (data) {
				if (data.action && ch_self.pushs[data.action])
					ch_self.pushs[data.action](data.params);
			});
			
			ch_self.client.onerror = function (err) {
				alert(err);
			}
		}catch (e){
			alert("" + e);
		}
		return this;
	},
	register_push: function (action, callback) {
		ch_self.pushs[action] = callback;
		return ch_self;
	},
	invoke: function (route, action, params, callback) {
		if (typeof params === "function") {
			callback = params;
			params = null;
		}
		ch_self.client.call({ route: route, action: action, params: params}, function (data) {
			if (callback) callback(data);
		}, function (err) {
			if (callback) callback(null);
		});
	}
};