var isLoad_jq = false;

try {
    if($ == undefined || $ == null) isLoad_jq = true;
}catch (e){
    isLoad_jq = true;
}

if (isLoad_jq){
    $ = function() {    //  jquery.....照搬！！！
        var copyIsArray,
            toString = Object.prototype.toString,
            hasOwn = Object.prototype.hasOwnProperty;
        var class2type = {
            '[object Boolean]' : 'boolean',
            '[object Number]' : 'number',
            '[object String]' : 'string',
            '[object Function]' : 'function',
            '[object Array]' : 'array',
            '[object Date]' : 'date',
            '[object RegExp]' : 'regExp',
            '[object Object]' : 'object'
        };
        var type = function(obj) {
            return obj == null ? String(obj) : class2type[toString.call(obj)] || "object";
        };
        var isWindow = function(obj) {
            return obj && typeof obj === "object" && "setInterval" in obj;
        };
        var isArray = Array.isArray || function(obj) {
                return type(obj) === "array";
            };
        var isPlainObject = function(obj) {
            if (!obj || type(obj) !== "object" || obj.nodeType || isWindow(obj))return false;
            if (obj.constructor && !hasOwn.call(obj, "constructor")
                && !hasOwn.call(obj.constructor.prototype, "isPrototypeOf")) {
                return false;
            }
            var key;
            for (key in obj) {}
            return key === undefined || hasOwn.call(obj, key);
        };
        var extend = function(deep, target, options) {
            for (var pName in options) {
                var src = target[pName];
                var copy = options[pName];
                if (target === copy) { continue; }
                if (deep && copy
                    && (isPlainObject(copy) || (copyIsArray = isArray(copy)))) {
                    if (copyIsArray) {
                        copyIsArray = false;
                        clone = src && isArray(src) ? src : [];
                    } else clone = src && isPlainObject(src) ? src : {};
                    target[pName] = extend(deep, clone, copy);
                } else if (copy !== undefined)
                    target[pName] = copy;
            }
            return target;
        };
        return { extend : extend };
    }();
}



Object.prototype.json = function () { return JSON.stringify(this); };

Array.prototype.del = function(dx) {
    if(dx > this.length) return ;
    this.splice(dx,1);
};

function getLocalTime(nS) {
	return new Date(parseInt(nS)).toLocaleString().replace(/年|月/g, "-").replace(/日/g, " ");
}

function getDateDiff(dateTimeStamp){
	var minute = 1000 * 60;
	var hour = minute * 60;
	var day = hour * 24;
	var halfamonth = day * 15;
	var month = day * 30;
	var now = new Date().getTime();
	var diffValue = now - dateTimeStamp;
	if(diffValue < 0){return;}
	var monthC =diffValue/month;
	var weekC =diffValue/(7*day);
	var dayC =diffValue/day;
	var hourC =diffValue/hour;
	var minC =diffValue/minute;
	if(monthC>=1) result="" + parseInt(monthC) + "月前";
	else if(weekC>=1) result="" + parseInt(weekC) + "周前";
	else if(dayC>=1) result=""+ parseInt(dayC) +"天前";
	else if(hourC>=1) result=""+ parseInt(hourC) +"小时前";
	else if(minC>=1) result=""+ parseInt(minC) +"分钟前";
	else result="刚刚";
	return result;
}

Array.prototype.delByProperty = function(name, val) {
    for (var i = 0; i < this.length; i++){
        var obj = this[i][name];
        if (obj === val) {
            delete obj;
            this.splice(i, 1);
        }
    }
};

Array.prototype.foreach = function (fn) {
    if (!fn) return;
    for (var i = 0; i < this.length; i++){
        if (fn(this[i], i, this) == false) return;
    }
};

Array.prototype.contains = function(item){
    return this.filter(function (x) { return x == item; }) > 0;
};

function S4() {
    return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
}

function gid() {
    return (S4()+S4()+S4()+S4()+S4()+S4()+S4()+S4());
}

function gid_4() {
    return S4();
}

function getNow() {
    return (new Date()).valueOf();
}

Object.prototype.NotEmpty = function () {
	return this != undefined && this != null && this != "";
};

Object.prototype.isArray = function () { return Object.prototype.toString.call(this) === '[object Array]'; };

Object.prototype.isString = function () { return Object.prototype.toString.call(this) === "[object String]"; };

function getNowLong() {
    return java.lang.System.currentTimeMillis();
}

Object.prototype.fieldNumber = function () {
    var num = 0;
    for (var key in this) num++;
    return num;
};

Object.prototype.clone = function(){
    return JSON.parse(JSON.stringify(this));
};

Object.prototype.extend = function (options) {
    for (var opt in this){
        var val = options[opt];
        var oval = this[opt];
        if (!val) continue;
        if (Object.prototype.toString.call(val) === Object.prototype.toString.call(oval)) {
            if (val.isArray()) {
                val.forEach(function (item) {
                    if (!oval.contains(item)) oval.push(item);
                });
            }else {
                if (val !== oval) this[opt] = val;
            }
        }
    }
};

String.prototype.replaceAll = function (exp, newStr) {
    return this.replace(new RegExp(exp, "gm"), newStr);
};

String.prototype.format = function(args) {
    var result = this;
    if (arguments.length < 1) {
        return result;
    }

    var data = arguments; // 如果模板参数是数组
    if (arguments.length == 1 && typeof (args) == "object") {
        // 如果模板参数是对象
        data = args;
    }
    for ( var key in data) {
        var value = data[key];
        if (undefined != value) {
            result = result.replaceAll("\\{" + key + "\\}", value);
        }
    }
    return result;
};