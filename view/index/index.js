//  函数声明2种语法，，1： function (){}  此种方式this为当前调用对象，在此处则为vm变量，，2: () => {}  此方式this为根调用对象，在此处为window对象
var vm = new Vue({
	el: 'body',
	data: {
		hp: cnynld_hprose.init().register_push("user_Accept", function (data) {
			vm.msg.push(data.msg);
		}),
		body: 'Hello Vue.indexx!',
		but1: false,
		bj:{ d: false, s: true, i: true},
		bodys: false,
		bodycolor: "red",
		item:"",
		list:[],
		msg: []
	},
	methods: {
		color_change: function () {
			this.bodycolor = this.bodycolor == "red" ? "yellow" : "red";
		},
		getlist:function () {
			var self = this;
			this.hp.invoke("index", "list", null, function (data) {
				self.list = data;
			});
		},
		showf:function () {
			layer.open({
				type: 2,
				title: '我只是跳出来给你关闭的',
				shadeClose: true,
				shade: 0.8,
				area: ['300px', '300px'],
				content: '/' //iframe的url
			});
		}
	}
});