module.exports = {
	get_property_number(obj) {
		var result = 0;
		for (let item in obj) result++;
		return result;
	},
	is_empty(obj) {
		return this.get_property_number(obj) == 0
	},
	readOnly (proxy_obj, init_obj = {}) {
		return new Proxy(init_obj, {
			set(target, propKey){ throw `object property:${propKey} the readOnly...!`;},   //  禁止设置属性
			get(target, propKey){
				try { return proxy_obj[propKey]; }catch (e){ return undefined; }   //  读属性代理
			}
		});
	},
	clone(source){
		return JSON.parse(JSON.stringify(source));
	},
	clone_func(target, source){
		if (!source) {
			source = target;
			target = {};
		}
		for (let key in source){
			if (typeof source[key] === "function") target[key] = source[key];
		}
		return target;
	}
};


