var fs = require("fs");

module.exports = {
	find_files: function (dirStr, file_regular, callback) {
		(function dir(dirpath) {
			fs.readdirSync(dirpath).forEachAsync((file) => {
				try {
					var path = dirpath + file;
					if (fs.statSync(path).isDirectory()) dir(path + '/');
					else {
						if (!file_regular.test(file)) return;
						callback && callback(path, dirpath.replace(dirStr, "/").replace(/(^\/)|(\/$)/g, ""), file.match(/(.*?)\.controller\.js/)[1]);
					}
				}catch (e){}
			});
		})(dirStr.replace(/\\/g, "/"));
	},
	read(path, callback){
		fs.readFile(path, (err, data) =>{
			callback(err ? null : data);
		});
	},
	read_binary(path, callback){
		fs.readFile(path, "binary", (err, data) =>{
			callback(err ? null : data);
		});
	}
};