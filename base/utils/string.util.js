var Buffer = require("buffer").Buffer;
var crypto = require("crypto");

module.exports = {
	toMd5: function (data) {
		return crypto.createHash("md5").update(new Buffer(data).toString("binary")).digest("hex");
	}
};