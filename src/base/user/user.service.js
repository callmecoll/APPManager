function save(json, callback) {
	var entity = $.getEntity();   //  获取user实体
	Object.assign(entity, json);   //  合并对象，默认合并到第一个对象
	console.log(entity.json());
	$.server.save(entity, (data) => {    //  存储数据库
		callback(data);
		if (data) $.hprose.push("user.save", entity);   //  调用hp服务进行推送
})
}

function getall(callback) {
	$.server.search({}, (data) => callback(data));
     //  取得mongodb数据库中所有数据并调用callback返回
}
function push_user_accept() {
	$.hprose.push("user_Accept", { msg: "有用户进入..!"});   //  调用hp服务进行推送
}

function get_page_list(json, callback) {
	$.server.page(json, (result) => callback(result));

}