var config = {
	cluster: "default",
	db: "FileService",
	col: "user"
};

class entity {
	constructor() {
		this.name = "def_name";
		this.sex = "";
		this.age = "";
		this.phone = "";
		this.qq = "";
	}
	
	json(){ //  定义toJson方法
		return JSON.stringify(this);
	}
	
	parse(obj){ //  定义parse方法
		for (var key in obj){
			if (this[key] !== undefined) this[key] = obj[key];
		}
	}
}

module.exports = entity;    //  导出该类