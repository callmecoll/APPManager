/**
 * Created by callmecool on 2016/10/27.
 */
var config = {
    cluster: "default",
    db: "LDServer-APP",
    col: "APPINFO"
};
module.exports = {};

class entity {
    constructor() {
        this.AppName = "";//APP名称
        this.AppClass = ""; //APP分类
        this.AppIcon = ""; //应用图标
        this.Describe = null; //描述
        this.IsEnable = true;//默认true  是否启用
        this.AppKey = null; //唯一ID 8位随机数字
        this.AppSecret = null; //应用密钥
        this.AppFlow = ""//应用流量 每天,
    };;
    json() { //  定义toJson方法
        return JSON.stringify(this);
    }
    parse(obj) { //  定义parse方法
        for (var key in obj) {
            if (this[key] !== undefined) this[key] = obj[key];
        }
    }
}
module.exports = entity;    //  导出该类

