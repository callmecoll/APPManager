/**
 * Created by callmecool on 2016/10/27.
 */
function save(json, callback) {
    var entity = $.getEntity();   //  获取user实体
    entity = Object.assign({}, entity, json);   //  合并对象
    $.server.save(entity, (data) => {    //  存储数据库
        callback(data);
        if (data) $.hprose.push("app.save", entity);   //  调用hp服务进行推送
})
}

function getall(callback) {
    $.server.search({}, (data) => callback(data));
    //  取得mongodb数据库中所有数据并调用callback返回
}
function get_page_list(json, callback) {
    $.server.page(json, (result) => callback(result));

}
